'use strict';
var clone =require("clone");
module.exports = function(doctor) {
    doctor.getDoctors = function(callback) {
        var doctorObject;
        const SPEC=doctor.app.models.speciality;
        doctor.find().then(async function(res){
            console.log(res)
            var specId=res[0].specialityId;
            var result=await SPEC.findById(specId);
            var responseObject=clone(res[0]);
            responseObject.specialityName=result.speciality;
            callback(null, responseObject);
        }).catch(async function(err){
            callback(null, err.message);
        })
        
      };
};
//{include:{relation:"speciality"}}